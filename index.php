<?php
    require_once("action/loginAction.php");
    require_once("partial/header.php");

	$action = new LoginAction();
	$action->execute();

	$wrongLogin = $action->wrongLogin;
?>

<script src='js/three.js/build/three.min.js'></script>
<script src='js/threex.planets.js'></script>

</head>
    <body>
    <script src='js/earth.js'></script>
        <div class="main"> 
            <div class="loginBox">
                <h1 class="login">Connexion</h1>
                <form action="index.php" method="post">
                    <div class="errorMsg" > <?= $wrongLogin ?> </div>
                    <div class="loginField">
                        <label class="login">Nom d'usager</label>
                        <input id="usrName" type="text" name="username" value="">
                    </div>
                    <div class="loginField">
                        <label class="login">Mot de passe</label>
                        <input id="pwd" type="password" name="pwd" value="">
                    </div>
                    <div>
                        <button id="btnJouer" type="submit" onclick="saveUsername()">Entrer</button>
                    </div>
                </form>
            </div>
        </div>
</body>