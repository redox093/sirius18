var ctx = null;
var canvasWidth;
var canvasHeight;
let sprite;
let music = new Audio("sound/interstellar.mp3");

$(function() {

	music.play();
    music.volume = 0.8;
	generalTick();
	
});	

function generalTick() {
    if (music.ended) music.play();
	window.requestAnimationFrame(generalTick);
}

function saveUsername() {
    localStorage["username"] = $("#usrName").val();
}

window.onload = function () {
    if (localStorage["username"] != null) {
        $("#usrName").val(localStorage["username"]);
    }
}
