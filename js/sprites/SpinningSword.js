class SpinningSword {
	constructor(x, y) {
        this.x = x;
        this.initialX = x;
        this.y = y;
        this.width = 50;
        this.height = 50;
        this.music = new Audio("sound/axe.mp3");
        this.count = 0;
        this.newSpinningSword = new Image()
        this.newSpinningSword.src = "images/sword.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

	tick() {

        this.count += 1;

        if (this.count % 2 == 1) {
            this.newSpinningSword.src = "images/swordRightUp.png"
        }
        else if (this.count % 3 == 1) {
            this.newSpinningSword.src = "images/swordRightDown.png"
        }
        else if (this.count % 4 == 1) {
            this.newSpinningSword.src = "images/swordLeftDown.png"
        }
        else if (this.count % 5 == 1) {
            this.newSpinningSword.src = "images/sword.png"
        }

        if (this.count > 100 && this.count < 300) {
            if(this.initialX <= 200) {
                this.x += 2;
                this.y -= 1;
            }
            else if (this.initialX > 200 && this.initialX <= 400) {
                this.x += 1;
                this.y -= 1;
            }
        }

        if (this.count > 300 && this.count < 600) {
            this.music.play();
            this.music.volume = 0.2;
        }

        if (this.newSpinningSword.complete) {
            this.ctx.drawImage(this.newSpinningSword, this.x, this.y, this.width, this.height);
        }

        return this.count < 600;

    }
}