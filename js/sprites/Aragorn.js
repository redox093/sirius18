class Aragorn {
	constructor() {
		this.x = 50;
        this.y = 300;
        this.width = 100;
        this.height = 130;
        this.newAragorn = new Image()
        this.newAragorn.src = "images/aragorn.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

    moveRight() {
        this.x += 5;
    }

    moveLeft() {
        this.x -= 5;
    }

	tick() {

        if (this.newAragorn.complete) {
            this.ctx.drawImage(this.newAragorn, this.x, this.y, this.width, this.height);
        }

        return nbPlayers >= 1;

    }
}