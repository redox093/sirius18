class Eagle {
	constructor() {
        this.x = 0;
        this.initialX = 0;
        this.y = 20;
        this.width = 100;
        this.height = 100;
        this.music = new Audio("sound/eagle.mp3");
        this.music.play();
        this.music.volume = 0.2;
        this.count = 0;
        this.newEagle = new Image()
        this.newEagle.src = "images/eagle.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

	tick() {

        this.count += 1;

        if (this.count > 0 && this.count < 250) {
            this.x += 2;
        }

        if (this.count % 5 == 0) {
            this.y += 1;
        }

        if (this.newEagle.complete) {
            this.ctx.drawImage(this.newEagle, this.x, this.y, this.width, this.height);
        }

        return this.count < 300;

    }
}