class Lightning {
	constructor() {
		this.x = 150;
        this.y = 40;
        this.width = 500;
        this.height = 500;
        this.music = new Audio("sound/thunder.mp3");
        this.music.play();
        this.music.volume = 0.2;
        this.count = 1000;
        this.newLightning = new Image()
        this.newLightning.src = "images/lightning.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

	tick() {

        if (this.count % 3 == 1) {
            this.newLightning.src = "images/lightning.png"
        }
        else this.newLightning.src = "images/blank.png"

        this.count -= 1;

        if (this.newLightning.complete) {
            this.ctx.drawImage(this.newLightning, this.x, this.y, this.width, this.height);
        }

        return this.count > 0;

    }
}