class Frodo {
	constructor() {
		this.x = 50;
        this.y = 300;
        this.width = 150;
        this.height = 150;
        this.newFrodo = new Image()
        this.newFrodo.src = "images/frodo.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

    moveRight() {
        this.x += 5;
    }

    moveLeft() {
        this.x -= 5;
    }

	tick() {

        if (this.newFrodo.complete) {
            this.ctx.drawImage(this.newFrodo, this.x, this.y, this.width, this.height);
        }

        return information.other_players.length >= 2;

    }
}