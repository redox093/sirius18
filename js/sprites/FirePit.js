class FirePit {

    // J'ai tenté de faire un rassemblement autour du feu au coeur du désert, mais le code du feu s'y prêtait mal

	constructor(x, y) {
        this.images = ["images/firePit/love.png", "images/firePit/guitarGirl.png", "images/firePit/laptopGirl.png", "images/firePit/ghandi.png", "images/firePit/picking.png", "images/firePit/smiling.png", "images/firePit/studying.png"];
		this.x = x;
        this.y = y;
        this.width = 100;
        this.height = 100;
        this.opacity = 0;
        this.dying = false;
        this.newPpl = new Image()
        this.newPpl.src = this.images[Math.floor(Math.random() * this.images.length - 1) + 1]
    }

	tick() {

        if (!this.dying) {
            this.opacity += 0.001;
            if (this.opacity >= 1) this.dying = true;
        }
        else this.opacity -= 0.001;

        if (this.newPpl.complete) {
            ctx.globalAlpha = this.opacity;
            ctx.drawImage(this.newPpl, this.x, this.y, this.width, this.height);
        }

        return this.opacity > 0;
    }
}