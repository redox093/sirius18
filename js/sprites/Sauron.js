class Sauron {
	constructor() {
		this.x = 500;
        this.y = 50;
        this.width = 120;
        this.height = 100;
        this.newBank = new Image()
        this.newBank.src = "images/OeilSauron.png"
        this.left = false;
        this.up = false;
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

    moveRight() {
        this.x += 1;
    }

    moveLeft() {
        this.x -= 1;
    }

    moveUp() {
        this.y -= 1;
    }

    moveDown() {
        this.y += 1;
    }

	tick() {
        
        if (this.x > 530) {
            this.left = true;
        }
        else if (this.x < 480) {
            this.left = false;
        }

        if (this.y > 100) {
           this.up = true;
        }
        else if (this.y < 0) {
            this.up = false;
        }

        if (Math.floor(Math.random() * 10) > 7) {
            if (this.up) this.moveUp();
            if (!this.up) this.moveDown();
            if (this.left) this.moveLeft();
            if (!this.left) this.moveRight();
        }

        if (this.newBank.complete) {
            this.ctx.drawImage(this.newBank, this.x, this.y, this.width, this.height);
        }

        return true;

    }
}