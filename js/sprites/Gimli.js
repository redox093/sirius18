class Gimli {
	constructor() {
		this.x = 220;
        this.y = 250;
        this.width = 150;
        this.height = 120;
        this.newGimli = new Image()
        this.newGimli.src = "images/gimli.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

    moveRight() {
        this.x += 5;
    }

    moveLeft() {
        this.x -= 5;
    }

	tick() {

        if (this.newGimli.complete) {
            this.ctx.drawImage(this.newGimli, this.x, this.y, this.width, this.height);
        }

        return nbPlayers >= 1;

    }
}