class Gandalf {
	constructor() {
		this.x = 120;
        this.y = 300;
        this.width = 150;
        this.height = 150;
        this.newGandalf = new Image()
        this.newGandalf.src = "images/gandalf.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

    moveRight() {
        this.x += 5;
    }

    moveLeft() {
        this.x -= 5;
    }

	tick() {

        if (this.newGandalf.complete) {
            this.ctx.drawImage(this.newGandalf, this.x, this.y, this.width, this.height);
        }

        return true;

    }
}