class Legolas {
	constructor() {
		this.x = 320;
        this.y = 300;
        this.width = 100;
        this.height = 120;
        this.newLegolas = new Image()
        this.newLegolas.src = "images/legolas.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

    moveRight() {
        this.x += 5;
    }

    moveLeft() {
        this.x -= 5;
    }

	tick() {

        if (this.newLegolas.complete) {
            this.ctx.drawImage(this.newLegolas, this.x, this.y, this.width, this.height);
        }

        return nbPlayers >= 0;

    }
}