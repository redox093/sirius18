class SpinningAxe {
	constructor(x, y) {
        this.x = x;
        this.initialX = x;
        this.y = y;
        this.width = 50;
        this.height = 50;
        this.music = new Audio("sound/axe.mp3");
        this.count = 0;
        this.newSpinningAxe = new Image()
        this.newSpinningAxe.src = "images/axe.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

	tick() {

        this.count += 1;

        if (this.count % 2 == 1) {
            this.newSpinningAxe.src = "images/axeRightUp.png"
        }
        else if (this.count % 3 == 1) {
            this.newSpinningAxe.src = "images/axeRightDown.png"
        }
        else if (this.count % 4 == 1) {
            this.newSpinningAxe.src = "images/axeLeftDown.png"
        }
        else if (this.count % 5 == 1) {
            this.newSpinningAxe.src = "images/axe.png"
        }

        if (this.count > 100 && this.count < 300) {
            if(this.initialX <= 200) {
                this.x += 2;
                this.y -= 1;
            }
            else if (this.initialX > 200 && this.initialX <= 400) {
                this.x += 1;
                this.y -= 1;
            }
        }

        if (this.count > 300 && this.count < 600) {
            this.music.play();
            this.music.volume = 0.2;
        }

        if (this.newSpinningAxe.complete) {
            this.ctx.drawImage(this.newSpinningAxe, this.x, this.y, this.width, this.height);
        }

        return this.count < 600;

    }
}