class Rock {
	constructor(x, y) {
        this.x = x;
        this.initialX = x;
        this.y = y;
        this.width = 50;
        this.height = 50;
        this.music = new Audio("sound/arrow.mp3");
        this.count = 0;
        this.newArrow = new Image()
        this.newArrow.src = "images/arrow"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

	tick() {

        this.count += 1;

        if (this.count > 50 && this.count < 150) {
            if(this.initialX <= 200) {
                this.x += 4;
                this.y -= 2;
            }
            else if (this.initialX > 200 && this.initialX <= 400) {
                this.x += 2;
                this.y -= 2;
            }
        }

        if (this.count > 150 && this.count < 300) {
            this.music.play();
            this.music.volume = 0.2;
        }

        if (this.newArrow.complete) {
            this.ctx.drawImage(this.newArrow, this.x, this.y, this.width, this.height);
        }

        return this.count < 300;

    }
}