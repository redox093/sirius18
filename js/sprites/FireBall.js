class FireBall {
	constructor(x, y) {
        this.x = x;
        this.initialX = x;
        this.y = y;
        this.width = 50;
        this.height = 50;
        this.music = new Audio("sound/explosion.mp3");
        this.count = 0;
        this.newFireball = new Image()
        this.newFireball.src = "images/fireballStatic.png"
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

	tick() {

        this.count += 1;

        if (this.count > 100 && this.count < 300) {
            this.newFireball.src = "images/fireballMoving.png"

            if(this.initialX <= 200) {
                this.x += 2;
                this.y -= 1;
            }
            else if (this.initialX > 200 && this.initialX <= 400) {
                this.x += 1;
                this.y -= 1;
            }
        }

        if (this.count > 300 && this.count < 500) {
            this.newFireball.src = "images/explosion.png"
            this.music.play();
            this.music.volume = 0.2;
        }

        if (this.newFireball.complete) {
            this.ctx.drawImage(this.newFireball, this.x, this.y, this.width, this.height);
        }

        return this.count < 500;

    }
}