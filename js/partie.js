let attacked = false, cooldownSkill = Date.now();
let nbPlayers = null;

$( function() {

        setTimeout( function refreshStatus () {

            let gameName, gameType, gameLevel, gameHp, gameMaxHp, playerName,playerLevel, playerType, skillName, skillCost, skillDmg, skillHeal, other_playerHp;
            let playerHp, playerMp, playerMaxHp, playerMaxMp, other_playerName, other_playerLevel, welcomeText, other_playerType, other_playerMaxHp, other_playerMaxMp, other_playerMp;
            
            jQuery.ajax({

                url:"ajax-game-status.php",
                type: "POST",
                data:{
                    "game":{
                        "name": gameName,
                        "type": gameType,
                        "level": gameLevel,
                        "hp": gameHp,
                        "attacked" : false,
                        "max_hp": gameMaxHp
                    },
                    "player":{
                        "name": playerName,
                        "level": playerLevel,
                        "type": playerType,
                        "skills": [
                            {
                            "name": skillName,
                            "cost": skillCost,
                            "dmg": skillDmg,
                            "heal": skillHeal
                            }
                        ],
                        "hp": playerHp,
                        "mp": playerMp,
                        "max_hp": playerMaxHp,
                        "max_mp": playerMaxMp
                    },
                    "other_players":[
                        {
                        "name": other_playerName,
                        "level": other_playerLevel,
                        "welcome_text": welcomeText,
                        "type": other_playerType,
                        "max_hp": other_playerMaxHp,
                        "max_mp": other_playerMaxMp,
                        "hp": other_playerHp,
                        "mp": other_playerMp,
                        "attacked": "--"
                        }
                    ]
                }
            })
            .done( function (data) {

                setTimeout(refreshStatus, 2000);
                let information = JSON.parse(data);

                // Vérifier si la partie est en cours, et sinon gagnée ou perdue
                if (information == "GAME_NOT_FOUND_WIN") {

                    document.getElementById("titleBox").innerHTML = "Vous avez gagné !  </br></br> <div><a href='accueil.php'> Retour a l'accueil </a></div>";
                    document.getElementById("skill0").style.display = "none";
                    document.getElementById("skill1").style.display = "none";
                    document.getElementById("skill2").style.display = "none";

                }
                else if (information == "GAME_NOT_FOUND_LOST") {

                    document.getElementById("titleBox").innerHTML = "Vous avez perdu...  </br></br> <div><a href='accueil.php'> Retour a l'accueil </a></div>";
                    document.getElementById("skill0").style.display = "none";
                    document.getElementById("skill1").style.display = "none";
                    document.getElementById("skill2").style.display = "none";

                }
                else {

                    // Populer la balise avec le titre de la partie
                    document.getElementById("titleBox").innerHTML = information.game.name;
    
                    // Populer les balises avec les attributs du joueur principal
                    document.getElementById("playerName").innerHTML = information.player.name;
                    document.getElementById("playerLevel").innerHTML = "Niveau : " + information.player.level;
                    document.getElementById("playerType").innerHTML = "Type : " + information.player.type;
                    document.getElementById("playerHp").innerHTML = "Vie : " + information.player.hp + "/" + information.player.max_hp;
                    document.getElementById("playerMp").innerHTML = "Mana : "+ information.player.mp + "/" + information.player.max_mp;
    
                    // Vérifier la présence d'autres joueurs et populer les balises correspondantes avec leurs attributs s'il y a lieu
                    if (information.other_players != undefined) {

                        nbPlayers = information.other_players.length;

                        for ( let i = 0; i < information.other_players.length; i++ ) {

                            document.getElementById("ally" + i + "Box").style.display = "block";
                            document.getElementById("ally" + i + "Name").innerHTML = information.other_players[i].name;
                            document.getElementById("ally" + i + "Level").innerHTML = information.other_players[i].level;
                            document.getElementById("ally" + i + "Type").innerHTML = information.other_players[i].type;
                            document.getElementById("ally" + i + "Hp").innerHTML = information.other_players[i].hp + "/" + information.other_players[i].max_hp;
                            document.getElementById("ally" + i + "Mp").innerHTML = information.other_players[i].mp + "/" + information.other_players[i].max_mp;

                            if (i == 0) {
                                spriteList.push(new Legolas());
                                if(information.other_players[i].attacked != "--") {
                                    let x, y;
                                    for (let i = 0; i < spriteList.length; i++) {
                                        if (spriteList[i] instanceof Legolas) {
                                            x = spriteList[i].x;
                                            y = spriteList[i].y;
                                        }
                                    }
                                    spriteList.push(new Arrow(x, y));
                                }
                            }

                            if (i == 1) {
                                spriteList.push(new Gimli());
                                if(information.other_players[i].attacked != "--") {
                                    let x, y;
                                    for (let i = 0; i < spriteList.length; i++) {
                                        if (spriteList[i] instanceof Gimli) {
                                            x = spriteList[i].x;
                                            y = spriteList[i].y;
                                        }
                                    }
                                    spriteList.push(new SpinningAxe(x, y));
                                }
                            }

                            if (i == 2) {
                                spriteList.push(new Aragorn());
                                if(information.other_players[i].attacked != "--") {
                                    let x, y;
                                    for (let i = 0; i < spriteList.length; i++) {
                                        if (spriteList[i] instanceof Gimli) {
                                            x = spriteList[i].x;
                                            y = spriteList[i].y;
                                        }
                                    }
                                    spriteList.push(new SpinningSword(x, y));
                                }
                            }

                        }

                    }
                    else {
                        document.getElementById("allyBox").style.display = "none";	
                    }

                    // Populer les balises avec les attributs de chaque skill
                    for ( let i = 0; i < information.player.skills.length; i++ ) {
                        if (information.player.skills[i] != undefined) {
                            document.getElementById("skill" + i).style.display = "block";
                            document.getElementById("skill" + i + "Name").innerHTML = information.player.skills[i].name;
                            document.getElementById("skill" + i + "Cost").innerHTML = "Mana cost: " + information.player.skills[i].cost;
                            document.getElementById("skill" + i + "Dmg").innerHTML = "Dmg: " + information.player.skills[i].dmg;
                            document.getElementById("skill" + i + "Heal").innerHTML = "Heal: " + information.player.skills[i].heal;
                        }
                    }
    
                    // Populer la balise du boss
                    document.getElementById("gameType").innerHTML = information.game.type;
                    document.getElementById("gameLevel").innerHTML = "Level: "+information.game.level;
                    document.getElementById("gameHp").innerHTML = information.game.hp + "/" + information.game.max_hp;

                    attacked = information.game.attacked;
                }
            })
        }, 2000);
    
        // Skills et leurs animations respectives
        document.getElementById("skill0").onclick = function() {
            setTimeout( function() {
                if ( Date.now() > cooldownSkill + 2000 ) {
                    jQuery.ajax({
                        url:"ajax-game-skills.php",
                        type: "POST",
                        data:{
                            "skill-name" : "Normal"
                        }
                    })
                    .done(function (data){
                        let information = JSON.parse(data);
                        cooldownSkill = Date.now();
                        if (information == "OK") {
                            let x, y;
                            for (let i = 0; i < spriteList.length; i++) {
                                if (spriteList[i] instanceof Gandalf) {
                                    x = spriteList[i].x + 50;
                                    y = spriteList[i].y;
                                }
                            }
                            spriteList.push(new FireBall(x, y));
                        }
                    })
                }
            }, 2000)
        }
    
        document.getElementById("skill1").onclick = function() {
            setTimeout(function() {
                if ( Date.now() > cooldownSkill + 2000 ) {
                    jQuery.ajax({
                        url:"ajax-game-skills.php",
                        type: "POST",
                        data:{
                            "skill-name" : "Special1"
                        }
                    })
                    .done(function (data) {
                        let information = JSON.parse(data);
                        cooldownSkill = Date.now();

                        if (information == "OK") {
                            let x, y;
                            for (let i = 0; i < spriteList.length; i++) {
                                if (spriteList[i] instanceof Gandalf) {
                                    x = spriteList[i].x + 50;
                                    y = spriteList[i].y;
                                }
                            }
                            spriteList.push(new FireBallBlue(x, y));
                        }

                    })
                }
            }, 2000)
        }
    
        document.getElementById("skill2").onclick = function() {
            setTimeout(function() {
                if ( Date.now() > cooldownSkill + 2000 ) {
                    jQuery.ajax({
                        url:"ajax-game-skills.php",
                        type: "POST",
                        data:{
                            "skill-name" : "Special2"
                        }
                    })
                    .done(function (data){
                        let information = JSON.parse(data);
                        cooldownSkill = Date.now();
                        if (information == "OK") {
                            spriteList.push(new Eagle());
                        }
   
                    })
                }
            }, 2000)
        }

        // Déplacement gauche et droite du personnage principal
        document.onkeydown = function (e) {
                if (e.which == 37) {
                    for (let i = 0; i < spriteList.length; i++) {
                        if (spriteList[i] instanceof Gandalf) {
                            spriteList[i].moveLeft();
                        }
                    }
                }
                else if (e.which == 39) {
                    for (let i = 0; i < spriteList.length; i++) {
                        if (spriteList[i] instanceof Gandalf) {
                            spriteList[i].moveRight();
                        }
                    }
                }
            }
    });	    