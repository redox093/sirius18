class Dude {
    constructor (x, y) {
        this.colCount = 23;
        this.rowCount = 7;
        this.refreshDelay = 100; 			// msec
        this.loopColumns = true;
        this.scale = 2.0;
        this.x = x;
        this.y = y;
        this.width = 100;
        this.height = 100;
        this.dead = false;
        this.newDude = new TiledImage("images/dudeSprite.png", this.colCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);
        this.newDude.changeRow(0);
        this.newDude.chagenMinMaxInterval(1, 4);
        this.ctx = document.getElementById("mycanvas").getContext("2d");
    }

    moveRight() {
        this.x += 5;
    }

    moveLeft() {
        this.x -= 5;
    }

	tick (left = false, right = false) {

        if (this.newDude.complete) {
            this.ctx.drawImage(this.newPpl, this.x, this.y, this.width, this.height);
        }

        return this.dead;
    }
}