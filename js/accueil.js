var ctx = null;
let spriteList = [];
let music = new Audio("sound/roadToAwe.mp3");
let showPers = false, showGames = false;

$(function() {

    // Musique
	music.play();
    music.volume = 0.8;

    // Recuperer le contexte, afficher le background et populer la spritelist
    /*canvas = document.getElementById("canvas");
    ctx = canvas.getContext("2d");

    canvasImg = canvas.toDataURL("image/png");
    background = new Image();
    background.src = canvasImg;*/
    
    // Afficher ou effacer les balises des informations du personnage
    document.getElementById("btnPersonnage").onclick = function() {
        if (!showPers) {
            document.getElementById("divContenu").style.display = "block";
            document.getElementById("personnageInfos").style.display = "block";
            document.getElementById("partiesInfos").style.display = "none"; 
            showPers = true; }
        else {
            document.getElementById("divContenu").style.display = "none";
            document.getElementById("personnageInfos").style.display = "none";
            document.getElementById("partiesInfos").style.display = "none"; 
            showPers = false; }
	}
    
    // Afficher ou effacer les balises des parties
    document.getElementById("btnParties").onclick = function() {
        if (!showGames) {
            document.getElementById("divContenu").style.display = "block";
            document.getElementById("personnageInfos").style.display = "none";
            document.getElementById("partiesInfos").style.display = "block";
            showGames = true; }
        else {
            document.getElementById("divContenu").style.display = "none";
            document.getElementById("personnageInfos").style.display = "none";
            document.getElementById("partiesInfos").style.display = "none";
            showGames = false; }
    }

    // Récupérer les informations du personnage et populer les balises
    function showInfosPersonnage() {

        let username, type, level, base_level_exp, next_level_exp, exp, victories, loss, last_game_state, unspent_points, unspent_skills, welcome_text, hp, mp, dodge_chance, dmg_red;

        $.ajax({

			url: "ajax-user-info.php",
			type: "POST",
			data: {
				"username" : username,
				"type" : type,
				"level" : level,
				"base_level_exp" : base_level_exp,
				"next_level_exp" : next_level_exp,
				"exp" : exp,
				"victories" : victories,
				"loss" : loss,
				"last_game_state" : last_game_state,
				"unspent_points" : unspent_points,
				"unspent_skills" : unspent_skills,
				"welcome_text" : welcome_text,
				"hp" : hp,
				"mp" : mp,
				"dodge_chance" : dodge_chance,
				"dmg_red" : dmg_red			
			}

		})
		.done (function (data) {

			let informations = JSON.parse(data);
			document.getElementById("sectionName").innerHTML = "Informations du joueur";
			document.getElementById("divNom").innerHTML = "Nom : " + informations.username;
			document.getElementById("divVie").innerHTML = "Vie : " + informations.hp;
            document.getElementById("divMana").innerHTML = "Mana : " + informations.mp;
            document.getElementById("divType").innerHTML = "Type : " + informations.type;
			document.getElementById("divNiveau").innerHTML = "Niveau : " + informations.level;
			document.getElementById("divXp").innerHTML = "Experience : " + informations.exp + "/"+ informations.next_level_exp;
			document.getElementById("divVictoires").innerHTML = "Victoires : " + informations.victories;
			document.getElementById("divDefaites").innerHTML = "Défaites : " + informations.loss;
			document.getElementById("divdDodgeChance").innerHTML = "Chance de feintes : " + informations.dodge_chance +" %";
            document.getElementById("divDmg").innerHTML = "Armure : " + informations.dmg_red +" %";
               
        })

    }

    // Récupérer les informations des parties et populer les balises
    function showInfosParties() {

        let name, level, current_hp, max_users, type, users, id;

        $.ajax({

            url:"ajax-game-info.php",
			type: "POST",
			data:{
				"name" : name,
				"level" : level,
				"current_hp" : current_hp,
				"max_users" : max_users,
				"type" : type,
				"users" : users,
				"id" : id
			}
	
		})
		.done(function (data) {

			let informations = JSON.parse(data);
			let text, newNode, newTextNode, parentNode, nbUsers;
	
			parentNode = document.getElementById("partiesInfos");
            parentNode.innerHTML = "<div id='partiesInfos'></div>"; 
                
			for (let i = 0; i < informations.length; i++) {

    			if (informations[i].users != undefined) { nbUsers = informations[i].users.length; }
				else { nbUsers = 0; }

                text = 	"<p>" + informations[i].name + "<br>" + "<span>" + informations[i].level + "</span>" + "<br>" + nbUsers + "/" 
                + informations[i].max_users +"<br>" + informations[i].current_hp +"<br>" + informations[i].type +"</p>";
                parentNode.innerHTML = parentNode.innerHTML + "<div id='partie_" + informations[i].id + "' class='gameBox' onclick='goToGame(" 
                + informations[i].id + ")'>" + text + "</div>";
                
            }
                
			parentNode.innerHTML = parentNode.innerHTML + "<div class='clear'></div>";
    
        })

    }

    showInfosPersonnage();
    showInfosParties();
    
    setInterval( function refresh() {

        showInfosPersonnage();
        showInfosParties();

	}, 5000 )

	generalTick();
	
});	

function goToGame(id) {

	setTimeout(function() {

		$.ajax({

			url:"ajax-game-enter.php",
			type: "POST",
			data: {
				id : id
            }
            
		})
        .done( function (data) {

            let information = JSON.parse(data);

			if ( information != "GAME_ENTERED" ) {
				document.getElementById("reponseErreur").style.display = "block";
				document.getElementById("reponseErreur").innerHTML = information; }
			else {
				window.location.replace("partie.php"); }
        })
        
	}, 2000 );

}

function generalTick() {

    if (music.ended) music.play();
    
    // Dessine le background a chaque fois pour éviter l'étalement des sprites sur l'image
    /*if (background.complete) {
        ctx.drawImage(background, 0, 0);
    }
    
    // Supprime les étoiles lorsqu'elles dépassent la limite droite du canvas
    for (let i = 0; i < spriteList.length; i++) {
        let alive = spriteList[i].tick();
    
        if (!alive) {
            spriteList.splice(i, 1); 
            i--;
        }
    }

    if (Math.floor(Math.random() * 500) > 498) {
        spriteList.push(new FirePit(randomX(), randomY()));
    }*/
    
    // Change l'affichage de l'écran
    window.requestAnimationFrame(generalTick);
}

function randomX () {
    let x = Math.floor(Math.random() * 600) + 100;
    return x;
}

function randomY () {
    let y = Math.floor(Math.random() * 300) + 200;
    return y;
}