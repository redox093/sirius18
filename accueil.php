<?php
	session_start();
?>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title>Sirius - Accueil</title>
		<script src="js/jquery.js" charset="utf-8"></script>
		<script src="js/accueil.js" charset="utf-8"></script>
		<script src="js/sprites/FirePit.js" charset="utf-8"></script>
		<script src="js/TiledImage.js"></script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/TweenMax.min.js'></script>
		<link rel="icon" href="images/lens.png" type="image/x-icon" />
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/accueil.css">
    </head>

    <body>
		
        <nav>
            <ul>
				<li id="btnPersonnage" class="button">Personnage</li>
				<li id="btnParties" class="button">Parties</li>
				<li id="btnLogout" class="button" onclick="window.location.replace('logout.php')">Déconnexion</li>
			</ul>
		</nav>

		<div id="divContenu" class ="contentBox" style="display:none">
			<div id="reponseErreur" style="display:none">Game_status</div>
				<div id="personnageInfos">	
					<div id="sectionName"></div>
					<div id="divNom"></div>
					<div id="divVie"></div>
					<div id="divMana"></div>
					<div id="divType"></div>
					<div id="divNiveau"></div>
					<div id="divXp"></div>
					<div id="divVictoires"></div>
					<div id="divDefaites"></div>
					<div id="divdDodgeChance"></div>
					<div id="divDmg"></div>
				</div>
			<div id="partiesInfos"></div>
		</div>

		<!--<canvas id="canvas" width="1000" height="600"></canvas>-->

		<div id="fire">
			<div id="f0" class="flame"></div>
		</div>

		<script src="js/sprites/fire.js" charset="utf-8"></script>
				
	</body>
	
</html>