<!DOCTYPE html>

<html lang="en">

	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Sirius - The Game</title>
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
		<link rel="icon" href="images/OeilSauron.png" type="image/x-icon" />

		<link rel="stylesheet" type="text/css" href="css/partie.css" >

		<script src="js/jquery.js" charset="utf-8"></script>
		<script src="js/partie.js" charset="utf-8"></script>

		<script src="js/sprites/Sauron.js" charset="utf-8"></script>
		<script src="js/sprites/Lightning.js" charset="utf-8"></script>

		<script src="js/sprites/Gandalf.js" charset="utf-8"></script>
		<script src="js/sprites/FireBall.js" charset="utf-8"></script>
		<script src="js/sprites/FireBallBlue.js" charset="utf-8"></script>
		<script src="js/sprites/Eagle.js" charset="utf-8"></script>

		<script src="js/sprites/Legolas.js" charset="utf-8"></script>
		<script src="js/sprites/Arrow.js" charset="utf-8"></script>
		
		<script src="js/sprites/Gimli.js" charset="utf-8"></script>
		<script src="js/sprites/SpinningAxe.js" charset="utf-8"></script>

		<script src="js/sprites/Frodo.js" charset="utf-8"></script>
		<script src="js/sprites/Aragorn.js" charset="utf-8"></script>
		<script src="js/sprites/SpinningSword.js" charset="utf-8"></script>

		<script type="text/javascript" src="js/canvasGame/oop.js"></script>
		<script type="text/javascript" src="js/canvasGame/tools.js"></script>
		<script type="text/javascript" src="js/canvasGame/cookie.js"></script>
		<script type="text/javascript" src="js/canvasGame/palette.js"></script>
		<script type="text/javascript" src="js/canvasGame/bitmap.js"></script>
		<script type="text/javascript" src="js/canvasGame/scenes.js"></script>
		<script type="text/javascript" src="js/canvasGame/tween.js"></script>
		<script type="text/javascript" src="js/canvasGame/framecount.js"></script>
		<script type="text/javascript" src="js/canvasGame/main.js"></script>
		
	</head>

	<body>
		
		<div id="container">
			<div id="d_header">
				<div id="d_scene_selector"></div>

				<div id="d_scene_btns">
					<div class="button left" onClick="CC.jumpScene(-1)">&lt; Prev</div><div class="button right" onClick="CC.jumpScene(1)">Next &gt;</div>
				</div>

				<div id="d_options_control">
					<div id="btn_options_toggle" class="button left right" onClick="CC.toggleOptions()">Ouvrir Menu &#x00BB;</div>
				</div>

				<div id="titleBox">Game Title</div>
				
				<div id="skill0" class="skills"  style="display:none;">
					<div id="skill0Name"></div>	
					<div id="skill0Cost"></div>	
					<div id="skill0Dmg"></div>	
					<div id="skill0Heal"></div>	
				</div>		
				<div id="skill1" class="skills" style="display:none;">
					<div id="skill1Name"></div>	
					<div id="skill1Cost"></div>	
					<div id="skill1Dmg"></div>	
					<div id="skill1Heal"></div>	
				</div>		
				<div id="skill2" class="skills" style="display:none;">
					<div id="skill2Name"></div>	
					<div id="skill2Cost"></div>	
					<div id="skill2Dmg"></div>	
					<div id="skill2Heal"></div>	
				</div>	

				
			</div>

			<div id="d_loading"></div>
			
			<canvas id="mycanvas" width="640" height="480" style=""></canvas>
			
			<div id="d_options">
				<div class="label">MUSIQUE:</div>

				<div class="section">
					<div id="btn_sound_off" class="button left" onClick="CC.setSound(false)">Off</div><div id="btn_sound_on" class="button right selected" onClick="CC.setSound(true)">On</div>
				</div>
				
				<div class="label">VITESSE DU CYCLE:</div>

				<div class="section">
					<div id="btn_speed_025" class="button thin left" onClick="CC.setSpeed(0.25)">&frac14;</div><div id="btn_speed_05" class="button thin" onClick="CC.setSpeed(0.5)">&frac12;</div><div id="btn_speed_1" class="button thin selected" onClick="CC.setSpeed(1)">1</div><div id="btn_speed_2" class="button thin" onClick="CC.setSpeed(2)">2</div><div id="btn_speed_4" class="button thin right" onClick="CC.setSpeed(4)">4</div>
				</div>
				
				<div class="label">STYLE DE CYCLE:</div>

				<div class="section">
					<div id="btn_blendshift_off" class="button left" onClick="CC.setBlendShift(false)">Standard</div><div id="btn_blendshift_on" class="button right selected" onClick="CC.setBlendShift(true)">Blend</div>
				</div>
				
				<div class="label">PALETTE:</div>
				<div id="palette_display"></div>
				
				<div id="d_debug"></div>

				<div id="bossBox">
					<div>Boss</div>
					<div id="gameType">Nothing to show</div>
					<div id="gameLevel">Nothing to show</div>
					<div id="gameHp">Nothing to show</div>
				</div>

				<div id="playerBox">
					<div id="playerName">Nothing to show</div>
					<div id="playerLevel">Nothing to show</div>
					<div id="playerType">Nothing to show</div>
					<div id="playerHp">Nothing to show</div>
					<div id="playerMp">Nothing to show</div>
				</div>

				<div id="ally0Box" class="allyBox" style="display: none;">
					<div id="ally0Name">Nothing to show</div>
					<div id="ally0Level">Nothing to show</div>
					<div id="ally0Type">Nothing to show</div>
					<div id="ally0Hp">Nothing to show</div>
					<div id="ally0Mp">Nothing to show</div>
				</div>

				<div id="ally1Box" class="allyBox" style="display: none;">
					<div id="ally1Name">Nothing to show</div>
					<div id="ally1Level">Nothing to show</div>
					<div id="ally1Type">Nothing to show</div>
					<div id="ally1Hp">Nothing to show</div>
					<div id="ally1Mp">Nothing to show</div>
				</div>

				<div id="ally2Box" class="allyBox" style="display: none;">
					<div id="ally2Name">Nothing to show</div>
					<div id="ally2Level">Nothing to show</div>
					<div id="ally2Type">Nothing to show</div>
					<div id="ally2Hp">Nothing to show</div>
					<div id="ally2Mp">Nothing to show</div>
				</div>

			</div>
			
			<div class="clear"></div>

		</div>
			
		<script language="JavaScript">

			if (document.addEventListener) {
				// Good browsers
				document.addEventListener( "DOMContentLoaded", function() {
					document.removeEventListener( "DOMContentLoaded", arguments.callee, false );
					CC.init();
				}, false );

				// Just in case
				window.addEventListener( "load", function() {
					window.removeEventListener( "load", arguments.callee, false );
					CC.init();
				}, false );
				
				window.addEventListener( "resize", function() {
					CC.handleResize();
				}, false );
			}

		</script>
		
	</body>
	
</html>