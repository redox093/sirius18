<?php 
	require_once("action/AjaxUserAction.php");

	$action = new AjaxUserAction();
	$action->execute();

	echo json_encode($action->result);